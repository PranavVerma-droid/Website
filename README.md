# 🌏 Website 🌏

Main Website Code.
Hosted using [Firebase](https://firebase.google.com).

All Main Code is in Inside [main](main).


Commands for Owner: <br> 
```npm install -g firebase-tools``` <br>
```firebase hosting:channel:deploy preview_1````

Tech Stack: <br>
My Tech Stack Is Really Simple Currently, which is Powering This Website. For the FrontEnd, I am using Vue.js and TailWindCSS. For the API's, I am using Zapier, which provides me with Free Email and is a really good alternative to IFTTT, and for the Backend, I am using Firebase for the Database, and Firebase Hosting to Deploy 🚀 💻
